package com.glassbeam.fsm

import akka.testkit.TestKit
import akka.testkit.ImplicitSender
import org.scalatest.BeforeAndAfterAll
import akka.actor.ActorSystem
import org.scalatest.Matchers
import org.scalatest.WordSpecLike
import akka.testkit.TestFSMRef
import scala.collection.immutable.HashMap

/**
 * @author prasannakumar
 */
class RealFSMTestSuite(as: ActorSystem) extends TestKit(as)
  with ImplicitSender
  with WordSpecLike
  with Matchers
  with BeforeAndAfterAll {

  def this() = this(ActorSystem("testActorSystem"))

  lazy val rulesFSMActor = TestFSMRef(new RealFSM())

  """The FSM actor """ should {

    """start with uninitialzed and move ot InitRule when Start msg""" in {
      rulesFSMActor.stateName shouldBe Idle
      rulesFSMActor ! Start
      rulesFSMActor.stateName shouldBe InitRule
    }

    """on InitRule when Initialize it should move to EvaluateRule""" in {
      rulesFSMActor ! Initialize
      rulesFSMActor.stateName shouldBe EvaluateRule
    }

    """on BundleDone msg evaluate rule it should move to TriggerRule""" in {
      rulesFSMActor ! RowMap(HashMap("" -> ""))
      rulesFSMActor ! BundleDone
      rulesFSMActor.stateName shouldBe TriggerRule
    }
    
    """on end should move from TriggerRule to Idle """ in {
      rulesFSMActor ! End
      rulesFSMActor.stateName.shouldBe(Idle)
    }
  }
}