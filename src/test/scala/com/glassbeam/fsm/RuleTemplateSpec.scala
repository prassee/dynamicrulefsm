package com.glassbeam.fsm

import org.scalatest._

/*
 * 
 */
class RuleTemplateSpec extends WordSpecLike with Matchers {

  // all these 4 comes from RulesTable
  val actorName = "A"
  val col1 = ""
  val col0 = ""
  val col2 = ""
  val col3 = ""
  val scope = ""
  val template = FSMCodeTemplate(actorName, col0, col1, col2, col3)

  """A RuleTemplate""" should {
    "generate valid Compilable Actor " in {
      template.shouldBe(template.ruleTemplate)
    }
  }
}
