package com.glassbeam.fsm

case class FSMCodeTemplateConfig(ruleId: String, cols: Array[String])

object FSMCodeTemplate {
  def apply(name: String, col0: String, col1: String, col2: String, col3: String) = {
    new FSMCodeTemplate(FSMCodeTemplateConfig(name, Array(col0, col1, col2, col3)))
  }
}

class FSMCodeTemplate(config: FSMCodeTemplateConfig) {

  val ruleTemplate = s"""
    import scala.collection.mutable.ListBuffer
    import akka.actor.Actor
    import akka.actor.Props
    import akka.actor.FSM
    import scala.collection.immutable.HashMap
    import com.glassbeam.fsm._
   
    class ActorName_${config.ruleId} extends Actor with FSM[State, Data] {
    import context._
    
    startWith(Idle, Uninitialized)
    
    ${config.cols(0)}
  
    when(Idle){
      case Event(Start, Uninitialized) =>
        println("-> Idle ")
        goto(InitRule)
    }
    
    when(InitRule) {
      case Event(Initialize, _) => {
        println("-> InitRule")
        ${config.cols(1)}
        goto(EvaluateRule)
      }
      case Event(_, _) =>
        println("InitRule, Not Handled")
        stay
    }
    
    when(EvaluateRule) {
      case Event(RowMap(m), _) => {
        println("-> EvaluateRule")
        ${config.cols(2)}
        stay
      }
      case Event(BundleDone, _) => {
        goto(TriggerRule)
      }
    }
  
    when(TriggerRule) {
      case Event(End, _) => {
        ${config.cols(3)}
        goto(Idle)
      }
    }
  
    whenUnhandled {
      case Event(e, s) =>
        println("State = Unhandled, Received unhandled request {} in state {}/{}" + e + stateName + s)
        stay
    }
    
    onTransition {
      case Idle -> InitRule => println(s"Moving from Idle to InitRule for rule ")
      case InitRule -> EvaluateRule => println(s"Moving from InitRule to EvaluateRule rule ")
    }
    
    initialize()
    
    }
    
    import akka.actor.ActorSystem
    val system = ActorSystem("mySystem")
    val myActor = system.actorOf(Props(new ActorName_${config.ruleId}()), name = "ActorName_${config.ruleId}")
    myActor
    """
}