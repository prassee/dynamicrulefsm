package com.glassbeam.fsm

import akka.actor.Actor
import akka.actor.FSM
import scala.collection.immutable.HashMap
import com.glassbeam.fsm._

sealed trait State
case object Idle extends State
case object InitRule extends State
case object EvaluateRule extends State
case object TriggerRule extends State

sealed trait Data
case object Uninitialized extends Data
case object Initialize extends Data
case object Start extends Data
case object BundleDone extends Data
case object CompilerFree extends Data
case object End extends Data
case class RowMap(m: HashMap[String, String]) extends Data

class RealFSM extends Actor with FSM[State, Data] {
  import context._

  startWith(Idle, Uninitialized)

  when(Idle) {
    case Event(Start, Uninitialized) =>
      println("-> Idle ")
      goto(InitRule)
  }

  when(InitRule) {
    case Event(Initialize, _) => {
      println("-> InitRule")
      goto(EvaluateRule)
    }
    case Event(_, _) => {
      println("InitRule, Not Handled")
      stay
    }
  }

  when(EvaluateRule) {
    case Event(RowMap(m), _) =>
      println("-> EvaluateRule")
      stay

    case Event(BundleDone, _) => {
      goto(TriggerRule)
    }
  }

  when(TriggerRule) {
    case Event(End, _) => {
      goto(Idle)
    }
  }

  whenUnhandled {
    case Event(e, s) =>
      println("State = Unhandled, Received unhandled request {} in state {}/{}" + e + stateName + s)
      stay
  }

  onTransition {
    case Idle -> InitRule => println(s"Moving from Idle to InitRule for rule ")
    case InitRule -> EvaluateRule => println(s"Moving from InitRule to EvaluateRule rule ")
  }

  initialize()

}

