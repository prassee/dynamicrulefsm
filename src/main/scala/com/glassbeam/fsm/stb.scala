package com.glassbeam.fsm

import scala.tools.reflect.ToolBox
import akka.actor.ActorRef

sealed trait Msg
case class Event1() extends Msg
case class Start1() extends Msg

/*
 * this code creates an actor from the rules definition 
 * we invoke the actor by sending a msg 
 */
object stb extends App {

  val tb = scala.reflect.runtime.universe.runtimeMirror(getClass.getClassLoader).mkToolBox()

  val actorName = "A"

  val col0 = """
    val x = ListBuffer[Int]()
    val y = ListBuffer[Int]()
    """

  val col1 = """
    x += 2
    y += 3
    """

  val ruleTemplate = s"""
    import scala.collection.mutable._
    import akka.actor.Actor
    import akka.actor.Props
    import com.glassbeam.fsm._

    class Rule_${actorName} extends Actor {
    
      import context._ 
    
      ${col0}
    
      def receive = {
        case Start =>
        println("start and becoming col1") 
        become(col1)
      }
      
      def col1:Receive = {
        case Event => 
        ${col1}
        println(x)
        println(y)
      }
    
    }
    
    import akka.actor.ActorSystem
    val system = ActorSystem("mySystem")
    val myActor = system.actorOf(Props[${actorName}], "${actorName}")
    myActor    
    """

  // printing the generated code for the RuleActor 
  println(ruleTemplate)

  // Iam using scala toolbox to parse the ruleTemplate
  val tree = tb.parse(ruleTemplate)

  // Iam casting the dynmically created actor as ActorRef  
  val actor: ActorRef = tb.eval(tree).asInstanceOf[ActorRef]

  // for demo purpose Iam just calling the actor 
  actor ! Start

  actor ! Event1
}